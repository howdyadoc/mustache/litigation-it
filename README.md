# mustache-litigation-it

Una collezione di "template partials" per mustache, destinati ad essere usati per l'attività giudiziale dell'avvocato all'interno del flusso di lavoro di howdyadoc.

**IMPORTANTE: devi mettere tutti i file `.mustache` in una sottocartella chiamata 'mustache' dentro la cartella dei sorgenti markdown**. Puoi copiarli manualmente in una sottocartella con quel nome o, se includi questo repo come git submodule nel tuo progetto, devi chiamare la cartella del submodulo (o il link alla medesima) 'mustache'.

Se vuoi sapere perché, leggi [README.geeky.md](README.geeky.md)

## Utilizzo

I "template partials" destinati ad essere utilizzati direttamente nel documento markdown sono:

- `{{> mustache/intestazione_completa}}`: intestazione per atti introduttivi, completa di tutti i dettagli delle parti e dei difensori

- `{{> mustache/intestazione_breve}}`: intestazione per atti endoprocedimentali, con la semplice indicazione dei nomi di parti e difensori

- `{{> mustache/clienti}}`: formula del tipo "Tizo e Caio, come sopra rappresentati e assistiti"

- `{{> mustache/_clienti}}`: desinenza di numero e genere per completare espressioni che si riferiscono ai clienti (ritorna `o`, `a`, `i`, `e` a seconda che ci si debba riferire ai clienti col singolare o plurale e col maschile o femminile)

- `{{> mustache/chiede}}`: ritorna CHIEDE o CHIEDONO (già formattato al centro e in grassetto) a seconda che si tratti di uno o più clienti

- `{{> mustache/domiciliatario}}`: ritorna nome (con articolo determinativo) e foro del domiciliatario

- `{{> mustache/intestazione_conclusioni}}`: formula canonica che precede le conclusioni

- `{{> mustache/firme_difensori}}`: da inserire alla fine dell'atto; impagina i nomi di tutti i difensori che firmano l'atto su una sola riga, allineati a destra-[centro]-[sinistra].

Tutti gli altri file sono principalmente dei sub-partial, anche se possono essere utilizzati direttamente nel documento markdown all'interno di altro "codice" mustache.

------------


A collection of mustache templates partials, intended to be used by Italian lawyers in legal litigation within the howdyadoc workflow. 

**IMPORTANT: you must put all `.mustache` files into a subdirectory named 'mustache' within your markdown source directory**. You may manually copy them into a subdirectory with that name or, if you include this repo as a git submodule in your project, you have to name the directory where this submodule is placed (or the link to it) 'mustache'. 

If you (really) want to know why, read [README.geeky.md](README.geeky.md).
