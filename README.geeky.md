## Why mustache files must be put in a subdirectory named 'mustache'?

Mustache partials and sub-partials (i.e. includes and sub-includes) are widely used here, because they allow to write clearer and more modular templates: you don't have to rewrite the same template parts many and many times, you get much more readable templates, and you can easily create new templates by reusing existing partials and sub-partials.

However, in this way you end up in having many little `.mustache` files: so it is natural to put them in a separate directory, other than your main source directory.

But this leads to a problem. In the howdyadoc workflow the 'main' mustache template is the markdown document located in the main source directory: if such main template includes partials located in a separate directory, and partials in turn need to include some sub-partials located in the same separate directory, such sub-partials cannot be found by the mustache command called from the main source directory -- unless one hardcodes their path in partials. This is because mustache, when including sub-partials, does *not* dynamically change the working path to the path of the firstly included partials.

It is easier to explain it with an example. Imagine you have the following directory structure:

```
.
├── mustache
│   ├── partial1.mustache
│   ├── partial2.mustache
│   ├── subpartialA.mustache
│   └── subpartialB.mustache
├── my_data.yaml
└── my_document.md

```

`my_document.md` has a line including `mustache/partial1.mustache`:

```
...
{{> mustache/partial1}}
...
```

(the `.mustache` extension is implicit and can be omitted)

However, `partial1.mustache` in turn needs to include sub-partials A and B at some point:

```
...
{{> subpartialA}}
{{> subpartialB}}
...
```

If you call the command: `mustache ../my_data.yaml partial1.mustache` from within the directory `mustache`, the above code works.

But we need to call the mustache command from the main source directory, in this way: `mustache my_data.yaml my_document.md`.

Parsing `my_document.md`, mustache will include `mustache/partial1.mustache`; and, parsing the latter, it will try to (sub-)include `subpartialA.mustache` and `subpartialB.mustache`, but it will not find them (this is because mustache does *not* change the working path to the path of the firstly included file).

So the working code for `partial1.mustache`, in our example, is:

```
...
{{> mustache/subpartialA}}
{{> mustache/subpartialB}}
...
```

In this way, the command `mustache my_data.yaml my_document.md` run in the main dir works, but we had to hardcode the path `mustache/` in `partial1.mustache`.

In conclusion, if one wants to place mustache partials and sub-partials in a different directory from the main source directory, the relative path of template sub-partials must be hardcoded in the template partials.

In this specific case, we arbitrarily chose to hardcode the path `mustache/` (we could have chosen `template/`, `tpl/` or the like, but `mustache/` seems more straightforward).
